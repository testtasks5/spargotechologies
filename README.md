Предлагаем Вам выполнить наше тестовое задание. Если у вас появятся
вопросы по его выполнению – сообщите, пожалуйста, об этом в ответном письме. 

Мы обязательно на них ответим!

1) CREATE TABLE #TEMP_GOODS (ID BIGINT, CODE VARCHAR(36), NAME
VARCHAR(256), PRODUCER VARCHAR(256))
CREATE CLUSTERED INDEX [IX_TGOODS$ID] ON #TEMP_GOODS
(
ID ASC
)
INSERT INTO #TEMP_GOODS (ID, CODE, NAME, PRODUCER)
SELECT TOP 1000000 ID, CODE, NAME, PRODUCER FROM GOODS

SELECT TOP 1000000 ID, CODE, NAME, PRODUCER
INTO #T_GOODS
FROM GOODS
CREATE CLUSTERED INDEX [IX_TGOODS$ID] ON #T_GOODS
(
ID ASC
)

Какой вариант вставки данных во временную таблицу более правильный и
почему?
1)1 2) 2 3) Оба правильные 4) Оба не правильные

2) Существует метод API, который принимает данные по нижеуказанной модели.
После первичной загрузки данных, они обрабатываются другими сервисами.
Появилась задача, включающая в себя 2 изменения:

-Для поля IsRemains необходимо добавить возможность передавать в значении не
только true и false, но и явный null, при этом, не лишаясь возможности не
передавать значение в это поле вообще.
т.е. в итоге должно появиться 4 возможных варианта (true, false, null и «не
передали»).
-Должно появиться новое поле - DateDefectStart а, поле DateDefect должно стать
полем DateDefectEnd, (при этом, не лишаясь возможности передавать его «по-
старому», как DateDefect). т.е. в итоге, обращаясь к методу API, должно быть
доступно передавать как одно поле DateDefect, так и 2 поля DateDefectStart и
DateDefectEnd, а на стороне сервисов обработки, поля DateDefect уже быть не
должно.
public class GoodsItemExtended : GoodsItemBase
{
//родительский код товара клиента
    [JsonProperty(Required = Required.DisallowNull, PropertyName = &quot;parent_code&quot;)]
    public string ParentCode { get; set; }
    //признак активности ассортиментного плана
    [JsonConverter(typeof(JsonGoodsIsActiveAssortConverter))]
    [JsonProperty(Required = Required.DisallowNull, PropertyName = &quot;is_active_assort&quot;)]
    public bool? IsActiveAssort { get; set; }
    //объем продаж, градация от 0 до 10
    [JsonConverter(typeof(JsonGoodsSalesVolumeConverter))]
    [JsonProperty(Required = Required.DisallowNull, PropertyName = &quot;sales_volume&quot;)]
    public int? SalesVolume { get; set; }
    //Наличие на остатке (да\нет)
    [JsonProperty(Required = Required.DisallowNull, PropertyName = &quot;is_remains&quot;)]
    public bool? IsRemains { get; set; }
    //Дата дефектуры.
    [JsonProperty(Required = Required.DisallowNull, PropertyName = &quot;date_defect &quot;)]
    public DateTime? DateDefect { get; set; };
}
3) Необходимо написать приложение на C# (вид не важен, консольное, или как
хочется), которое будет выполнять авторизацию и скачивать целиком всю
номенклатуру по любому из доступных демонстрационной учетной записи
департаментов (аптек)
метод - /Goods/{depId} (/User/departments), сваггер -
http://f3bus.test.pharmadata.ru/swagger/index.html
Сохранять результат можно куда угодно (например, в файлы на диск).